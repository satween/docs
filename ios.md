# iOS Development summary (Swift) #


## Basics

### Constants with variables
```
#!swift
let a = 0; // let - for final values
var b = 0; // var - for variable
let explicitDouble: Double = 70 // const with type

```
### Casting
Values are never implicitly converted to another type. If you need to convert a value to a different type, explicitly make an instance of the desired type.

```
#!swift

let label = "The width is "
let width = 94
let widthLabel = label + String(width)

```
There�s an even simpler way to include values in strings: Write the value in parentheses, and write a backslash (\) before the parentheses. For example:

```
#!swift
let apples = 3
let oranges = 5
let appleSummary = "I have \(apples) apples."
let fruitSummary = "I have \(apples + oranges) pieces of fruit."
```
### Arrays and dictionaries

```
#!swift
var shoppingList = ["catfish", "water", "tulips", "blue paint"]
shoppingList[1] = "bottle of water"

var occupations = [
    "Malcolm": "Captain",
    "Kaylee": "Mechanic",
]
```
To create an empty array or dictionary, use the initializer syntax.

```
#!swift
let emptyArray = [String]()
let emptyDictionary = [String: Float]()
```
