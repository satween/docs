# Adroid inerview summary #

## Services ##

### Thread ###

By default, a service runs in the same process as the main thread of the application.

Can be called in a separate process:
android:process=":my_process" // : - means private to declaring application
+ no ANR 
- IPC 
networking should be made under separate thread


### System priority ###

Services run with a higher priority than inactive or invisible activities and therefore it is less likely that the Android system terminates them.
A foreground service is a service that should have the same priority as an active activity and therefore should not be killed by the Android system, even if the system is low on memory

### How to start? ###

If the startService(intent) method is called and the service is not yet running, the service object is created and the onCreate() method of the service is called.

Once the service is started, the onStartCommand(intent) method in the service is called. It passes in the Intent object from the startService(intent) call.

In its onStartCommand() method call, the service returns an int which defines its restart behavior in case the service gets terminated by the Android platform.

* **Service.START_STICKY** - Service is restarted if it gets terminated. Intent data passed to the onStartCommand method is null. Used for services which manages their own state and do not depend on the Intent data.
* **Service.START_NOT_STICKY** - Service is not restarted. Used for services which are periodically triggered anyway. The service is only restarted if the runtime has pending startService() calls since the service termination.
* **Service.START_REDELIVER_INTENT** - Similar to Service.START_STICKY but the original Intent is re-delivered to the onStartCommand method.


### Binding ###
A client can bind to a service by calling bindService(Intent, ServiceConnection)
Multiple clients can connect to a service simultaneously. However, the system calls your service's onBind() method to retrieve the IBinder only when the first client binds

When the last client unbinds from the service, the system destroys the service, unless the service was also started by startService().


### How to stop? ###

You stop a service via the stopService() method. No matter how frequently you called the startService(intent) method, one call to the stopService() method stops the service.

A service can terminate itself by calling the stopSelf() method. This is typically done if the service finishes its work.


### IntentSerice ###
IntentService is a base class for Services that handle asynchronous requests (expressed as Intents) on demand. Clients send requests through startService(Intent) calls; the service is started as needed, handles each Intent in turn using a worker thread, and stops itself when it runs out of work.


## Application ##

The application object is created before any of your Android components are started. If you do not specify one in your AndroidManifest.xml file, the Android system creates a default object for you. It is started in a new process with a unique ID under a unique user. This object provides the following main life-cycle methods:

* **onCreate()** - called before the first components of the application starts

* **onLowMemory()** - called when the Android system requests that the application cleans up memory

* **onTrimMemory()** - called when the Android system requests that the application cleans up memory. This message includes an indicator in which position the application is. For example the constant TRIM_MEMORY_MODERATE indicates that the process is around the middle of the background LRU list; freeing memory can help the system keep other processes running later in the list for better overall performance.

* **onTerminate()** - only for testing, not called in production

* **onConfigurationChanged()** - called whenever the configuration changes



## Activities ##

### System priority ###

1. **Foreground** An application in which the user is interacting with an activity, or which has an service which is bound to such an activity. Also if a service is executing one of its lifecycle methods or a broadcast receiver which runs its onReceive() method.

2. **Visible** User is not interacting with the activity, but the activity is still (partially) visible or the application has a service which is used by a inactive but visible activity.

3. **Service** Application with a running service which does not qualify for 1 or 2.

4. **Background** Application with only stopped activities and without a service or executing receiver. Android keeps them in a least recent used (LRU) list and if requires terminates the one which was least used.

5. **Empty** Application without any active components.

### Life cycle ###

1. **Running** Activity is visible and interacts with the user.
2. **Paused** Activity is still visible but partially obscured, instance is running but might be killed by the system. For example covered by dialog.
3. **Stopped** Activity is not visible, instance is running but might be killed by the system.
4. **Killed** Activity has been terminated by the system of by a call to its finish() method.

![xactivity_lifecycle10.png](https://bitbucket.org/repo/oGdnjo/images/3219732202-xactivity_lifecycle10.png)

1. **onCreate()** Called then the activity is created. Used to initialize the activity, for example create the user interface.
2. **onResume()** Called if the activity get visible again and the user starts interacting with the activity again. Used to initialize fields, register listeners, bind to services, etc.
3. **onPause()** Called once another activity gets into the foreground. Always called before the activity is not visible anymore. Used to release resources or save application data. For example you unregister listeners, intent receivers, unbind from services or remove system service listeners.
4. **onStop()** Called once the activity is no longer visible. Time or CPU intensive shut-down operations, such as writing information to a database should be down in the onStop() method. This method is guaranteed to be called as of API 11.

### Saving state ###

As your activity begins to stop, the system calls the **onSaveInstanceState()** method so your activity can save state information with a collection of key-value pairs. The default implementation of this method saves transient information about the state of the activity's view hierarchy, such as the text in an EditText widget or the scroll position of a ListView widget. Your app should implement the **onSaveInstanceState()** callback after the **onPause()** method, and before onStop(). Do not implement this callback in **onPause()**.

In order for the Android system to restore the state of the views in your activity, each view must have a unique ID, supplied by the android:id attribute.

**onSaveInstanceState()** by default is not called when user clicks on back button.

### Restoring state ###

When your activity is recreated after it was previously destroyed, you can recover your saved state from the Bundle that the system passes to your activity. Both the **onCreate()** and **onRestoreInstanceState()** callback methods receive the same Bundle that contains the instance state information

Because the **onCreate()** method is called whether the system is creating a new instance of your activity or recreating a previous one, you must check whether the state Bundle is null before you attempt to read it. If it is null, then the system is creating a new instance of the activity, instead of restoring a previous one that was destroyed.

Instead of restoring the state during **onCreate()** you may choose to implement onRestoreInstanceState(), which the system calls after the **onStart()** method. The system calls **onRestoreInstanceState()** only if there is a saved state to restore, so you do default not need to check whether the Bundle is null

### Activity State Changes ###

There are a number of events that can trigger a configuration change. Perhaps the most prominent example is a change between portrait and landscape orientations. Other cases that can cause configuration changes include changes to language or input device.

### Tasks ###

A task is a collection of activities that users interact with when performing a certain job. The activities are arranged in a stack (the back stack), in the order in which each activity is opened. When apps are running simultaneously in a multi-windowed environment, supported in Android 7.0 (API level 24) and higher, the system manages tasks separately for each window; each window may have multiple tasks.

The device Home screen is the starting place for most tasks. When the user touches an icon in the application launcher (or a shortcut on the Home screen), that application's task comes to the foreground. If no task exists for the application (the application has not been used recently), then a new task is created and the "main" activity for that application opens as the root activity in the stack.

When the current activity starts another, the new activity is pushed on the top of the stack and takes focus. The previous activity remains in the stack, but is stopped. When an activity stops, the system retains the current state of its user interface. When the user presses the Back button, the current activity is popped from the top of the stack (the activity is destroyed) and the previous activity resumes (the previous state of its UI is restored). Activities in the stack are never rearranged, only pushed and popped from the stack—pushed onto the stack when started by the current activity and popped off when the user leaves it using the Back button. As such, the back stack operates as a "last in, first out" object structure.

If the user continues to press Back, then each activity in the stack is popped off to reveal the previous one, until the user returns to the Home screen (or to whichever activity was running when the task began). When all activities are removed from the stack, the task no longer exists.

A task is a cohesive unit that can move to the "background" when users begin a new task or go to the Home screen, via the Home button. While in the background, all the activities in the task are stopped, but the back stack for the task remains intact—the task has simply lost focus while another task takes place. A task can then return to the "foreground" so users can pick up where they left off. Suppose, for example, that the current task (Task A) has three activities in its stack—two under the current activity. The user presses the Home button, then starts a new application from the application launcher. When the Home screen appears, Task A goes into the background. When the new application starts, the system starts a task for that application (Task B) with its own stack of activities. After interacting with that application, the user returns Home again and selects the application that originally started Task A. Now, Task A comes to the foreground—all three activities in its stack are intact and the activity at the top of the stack resumes. At this point, the user can also switch back to Task B by going Home and selecting the application icon that started that task (or by selecting the app's task from the Recents screen). This is an example of multitasking on Android.

To summarize the default behavior for activities and tasks:

* When Activity A starts Activity B, Activity A is stopped, but the system retains its state (such as scroll position and text entered into forms). If the user presses the Back button while in Activity B, Activity A resumes with its state restored.
* When the user leaves a task by pressing the Home button, the current activity is stopped and its task goes into the background. The system retains the state of every activity in the task. If the user later resumes the task by selecting the launcher icon that began the task, the task comes to the foreground and resumes the activity at the top of the stack.
* If the user presses the Back button, the current activity is popped from the stack and destroyed. The previous activity in the stack is resumed. When an activity is destroyed, the system does not retain the activity's state.
* Activities can be instantiated multiple times, even from other tasks.

LAUNCH MODES:

* ** standard ** Default. The system creates a new instance of the activity in the task from which it was started and routes the intent to it. The activity can be instantiated multiple times, each instance can belong to different tasks, and one task can have multiple instances.

* ** singleTop ** If an instance of the activity already exists at the top of the current task, the system routes the intent to that instance through a call to its onNewIntent() method, rather than creating a new instance of the activity. The activity can be instantiated multiple times, each instance can belong to different tasks, and one task can have multiple instances (but only if the activity at the top of the back stack is not an existing instance of the activity). For example, suppose a task's back stack consists of root activity A with activities B, C, and D on top (the stack is A-B-C-D; D is on top). An intent arrives for an activity of type D. If D has the default "standard" launch mode, a new instance of the class is launched and the stack becomes A-B-C-D-D. However, if D's launch mode is "singleTop", the existing instance of D receives the intent through onNewIntent(), because it's at the top of the stack—the stack remains A-B-C-D. However, if an intent arrives for an activity of type B, then a new instance of B is added to the stack, even if its launch mode is "singleTop".

* ** singleTask ** The system creates a new task and instantiates the activity at the root of the new task. However, if an instance of the activity already exists in a separate task, the system routes the intent to the existing instance through a call to its onNewIntent() method, rather than creating a new instance. Only one instance of the activity can exist at a time.
Note: Although the activity starts in a new task, the Back button still returns the user to the previous activity.

* ** singleInstance ** Same as "singleTask", except that the system doesn't launch any other activities into the task holding the instance. The activity is always the single and only member of its task; any activities started by this one open in a separate task.

As another example, the Android Browser application declares that the web browser activity should always open in its own task—by specifying the singleTask launch mode in the <activity> element. This means that if your application issues an intent to open the Android Browser, its activity is not placed in the same task as your application. Instead, either a new task starts for the Browser or, if the Browser already has a task running in the background, that task is brought forward to handle the new intent.

Regardless of whether an activity starts in a new task or in the same task as the activity that started it, the Back button always takes the user to the previous activity. However, if you start an activity that specifies the singleTask launch mode, then if an instance of that activity exists in a background task, that whole task is brought to the foreground. At this point, the back stack now includes all activities from the task brought forward, at the top of the stack.

![diagram_backstack_singletask_multiactivity.png](https://bitbucket.org/repo/oGdnjo/images/1942467394-diagram_backstack_singletask_multiactivity.png)

The behaviours that you specify for your activity with the launchMode attribute can be overridden by flags included with the intent that start your activity.

INTENT FLAGS:

* ** FLAG_ACTIVITY_NEW_TASK ** 
Start the activity in a new task. If a task is already running for the activity you are now starting, that task is brought to the foreground with its last state restored and the activity receives the new intent in onNewIntent().
This produces the same behavior as the ** singleTask **  launchMode value, discussed in the previous section.

* ** FLAG_ACTIVITY_SINGLE_TOP **
If the activity being started is the current activity (at the top of the back stack), then the existing instance receives a call to onNewIntent(), instead of creating a new instance of the activity.
This produces the same behavior as the ** singleTop ** launchMode value, discussed in the previous section.

* ** FLAG_ACTIVITY_CLEAR_TOP ** 
If the activity being started is already running in the current task, then instead of launching a new instance of that activity, all of the other activities on top of it are destroyed and this intent is delivered to the resumed instance of the activity (now on top), through onNewIntent()).
There is no value for the launchMode attribute that produces this behavior.

If the launch mode of the designated activity is "standard", it too is removed from the stack and a new instance is launched in its place to handle the incoming intent. That's because a new instance is always created for a new intent when the launch mode is "standard".


### Multi-Window Support ###

Multi-window mode does not change the activity lifecycle.

In multi-window mode, only the activity the user has most recently interacted with is active at a given time. This activity is considered topmost. All other activities are in the paused state, even if they are visible.

If your app targets API level 24 or higher, you can configure how and whether your app's activities support multi-window display. You can set attributes in your manifest to control both size and layout. A root activity's attribute settings apply to all activities within its task stack. For example, if the root activity has android:resizeableActivity set to true, then all activities in the task stack are resizable.


## Fragments ##

A Fragment represents a behavior or a portion of user interface in an Activity. You can combine multiple fragments in a single activity to build a multi-pane UI and reuse a fragment in multiple activities. You can think of a fragment as a modular section of an activity, which has its own lifecycle, receives its own input events, and which you can add or remove while the activity is running (sort of like a "sub activity" that you can reuse in different activities).

A fragment must always be embedded in an activity and the fragment's lifecycle is directly affected by the host activity's lifecycle. For example, when the activity is paused, so are all fragments in it, and when the activity is destroyed, so are all fragments. However, while an activity is running (it is in the resumed lifecycle state), you can manipulate each fragment independently, such as add or remove them. When you perform such a fragment transaction, you can also add it to a back stack that's managed by the activity—each back stack entry in the activity is a record of the fragment transaction that occurred. The back stack allows the user to reverse a fragment transaction (navigate backwards), by pressing the Back button.

When you add a fragment as a part of your activity layout, it lives in a ViewGroup inside the activity's view hierarchy and the fragment defines its own view layout. You can insert a fragment into your activity layout by declaring the fragment in the activity's layout file, as a <fragment> element, or from your application code by adding it to an existing ViewGroup. However, a fragment is not required to be a part of the activity layout; you may also use a fragment without its own UI as an invisible worker for the activity.

Android introduced fragments in Android 3.0 (API level 11), primarily to support more dynamic and flexible UI designs on large screens, such as tablets. Because a tablet's screen is much larger than that of a handset, there's more room to combine and interchange UI components. Fragments allow such designs without the need for you to manage complex changes to the view hierarchy. By dividing the layout of an activity into fragments, you become able to modify the activity's appearance at runtime and preserve those changes in a back stack that's managed by the activity.

![fragments.png](https://bitbucket.org/repo/oGdnjo/images/1258739182-fragments.png)

### Life cycle ###
A fragment has its own life cycle. But it is always connected to the life cycle of the activity which uses the fragment.
If an activity stops, its fragments are also stopped. If an activity is destroyed, its fragments are also destroyed.


* ** onAttach() ** The fragment instance is associated with an activity instance.The fragment and the activity is not fully initialized. Typically you get in this method a reference to the activity which uses the fragment for further initialization work.
* ** onCreate() ** Fragment is created. The ** onCreate() ** method is called after the ** onCreate() ** method of the activity but before the ** onCreateView() ** method of the fragment.
* ** onCreateView() ** The fragment instance creates its view hierarchy. In the ** onCreateView() ** method the fragment creates its user interface. Here you can inflate a layout via the ** inflate() ** method call of the Inflator object passed as a parameter to this method.
In this method you should not interactive with the activity, the activity is not yet fully initialized.
There is no need to implement this method for headless fragments.The inflated views become part of the view hierarchy of its containing activity.
* ** onActivityCreated() ** The onActivityCreated() is called after the onCreateView() method when the host activity is created.
Activity and fragment instance have been created as well as the view hierarchy of the activity. At this point, view can be accessed with the findViewById() method. example.
In this method you can instantiate objects which require a Context object.
* ** onStart() ** The onStart() method is called once the fragment gets visible.
* ** onResume() ** Fragment becomes active.
* ** onPause() ** Fragment is visible but becomes not active anymore, e.g., if another activity is animating on top of the activity which contains the fragment.
* ** onStop() ** Fragment becomes not visible.
* **onDestroyView() ** Destroys the view of the fragment. If the fragment is recreated from the backstack this method is called and afterwards the onCreateView method.
* ** onDestroy() ** Not guaranteed to be called by the Android platform.

![fragment_lifecycle.png](https://bitbucket.org/repo/oGdnjo/images/2714724747-fragment_lifecycle.png)

### Performing Fragment Transactions ###

A great feature about using fragments in your activity is the ability to add, remove, replace, and perform other actions with them, in response to user interaction. Each set of changes that you commit to the activity is called a transaction and you can perform one using APIs in FragmentTransaction. You can also save each transaction to a back stack managed by the activity, allowing the user to navigate backward through the fragment changes (similar to navigating backward through activities).


```
#!java

// Create new fragment and transaction
Fragment newFragment = new ExampleFragment();
FragmentTransaction transaction = getFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack
transaction.replace(R.id.fragment_container, newFragment);
transaction.addToBackStack(null);

// Commit the transaction
transaction.commit();
```

In this example, newFragment replaces whatever fragment (if any) is currently in the layout container identified by the R.id.fragment_container ID. By calling **addToBackStack() **, the replace transaction is saved to the back stack so the user can reverse the transaction and bring back the previous fragment by pressing the Back button.

If you add multiple changes to the transaction (such as another **add()** or **remove()**) and call **addToBackStack()**, then all changes applied before you call **commit()** are added to the back stack as a single transaction and the Back button will reverse them all together.

The order in which you add changes to a FragmentTransaction doesn't matter, except:

* You must call ** commit() ** last
* If you're adding multiple fragments to the same container, then the order in which you add them determines the order they appear in the view hierarchy

If you do not call ** addToBackStack() ** when you perform a transaction that removes a fragment, then that fragment is destroyed when the transaction is committed and the user cannot navigate back to it. Whereas, if you do call ** addToBackStack() ** when removing a fragment, then the fragment is stopped and will be resumed if the user navigates back.

Calling ** commit() ** does not perform the transaction immediately. Rather, it schedules it to run on the activity's UI thread (the "main" thread) as soon as the thread is able to do so. If necessary, however, you may call ** executePendingTransactions() ** from your UI thread to immediately execute transactions submitted by ** commit() **. Doing so is usually not necessary unless the transaction is a dependency for jobs in other threads.

## Intents ##

![intent-filters@2x.png](https://bitbucket.org/repo/oGdnjo/images/3480003911-intent-filters@2x.png)

An Intent is a messaging object you can use to request an action from another app component. Although intents facilitate communication between components in several ways, there are three fundamental use cases:

* ** Starting an activity **
An Activity represents a single screen in an app. You can start a new instance of an Activity by passing an Intent to **startActivity()**. The Intent describes the activity to start and carries any necessary data.
If you want to receive a result from the activity when it finishes, call **startActivityForResult()**. Your activity receives the result as a separate Intent object in your activity's **onActivityResult()** callback. For more information, see the Activities guide.
* ** Starting a service **
A Service is a component that performs operations in the background without a user interface. With Android 5.0 (API level 21) and later, you can start a service with JobScheduler.
For versions earlier than Android 5.0 (API level 21), you can start a service by using methods of the Service class. You can start a service to perform a one-time operation (such as downloading a file) by passing an Intent to **startService()**. The Intent describes the service to start and carries any necessary data.
If the service is designed with a client-server interface, you can bind to the service from another component by passing an Intent to **bindService()**. For more information, see the Services guide.
* ** Delivering a broadcast **
A broadcast is a message that any app can receive. The system delivers various broadcasts for system events, such as when the system boots up or the device starts charging. You can deliver a broadcast to other apps by passing an Intent to **sendBroadcast()** or **sendOrderedBroadcast()**.

### Intent types ###

* ** Explicit ** intents specify the component to start by name (the fully-qualified class name). You'll typically use an explicit intent to start a component in your own app, because you know the class name of the activity or service you want to start. For example, you can start a new activity in response to a user action or start a service to download a file in the background. An explicit intent is always delivered to its target, regardless of any intent filters the component declares.
```
#!java

// Executed in an Activity, so 'this' is the Context
// The fileUrl is a string URL, such as "http://www.example.com/image.png"
Intent downloadIntent = new Intent(this, DownloadService.class);
downloadIntent.setData(Uri.parse(fileUrl));
startService(downloadIntent);
```

* ** Implicit ** intents do not name a specific component, but instead declare a general action to perform, which allows a component from another app to handle it. For example, if you want to show the user a location on a map, you can use an implicit intent to request that another capable app show a specified location on a map.** Caution **: To ensure that your app is secure, always use an explicit intent when starting a Service and do not declare intent filters for your services. Using an implicit intent to start a service is a security hazard because you can't be certain what service will respond to the intent, and the user can't see which service starts. Beginning with Android 5.0 (API level 21), the system throws an exception if you call bindService() with an implicit intent. 
```
#!java

// Create the text message with a string
Intent sendIntent = new Intent();
sendIntent.setAction(Intent.ACTION_SEND);
sendIntent.putExtra(Intent.EXTRA_TEXT, textMessage);
sendIntent.setType("text/plain");

// Verify that the intent will resolve to an activity
if (sendIntent.resolveActivity(getPackageManager()) != null) {
    startActivity(sendIntent);
}
```


### Building intents ###

An Intent object carries information that the Android system uses to determine which component to start (such as the exact component name or component category that should receive the intent), plus information that the recipient component uses in order to properly perform the action (such as the action to take and the data to act upon).

The primary information contained in an Intent is the following:

* ** Component name ** The name of the component to start.
This is optional, but it's the critical piece of information that makes an intent explicit, meaning that the intent should be delivered only to the app component defined by the component name. Without a component name, the intent is implicit and the system decides which component should receive the intent based on the other intent information (such as the action, data, and category—described below). If you need to start a specific component in your app, you should specify the component name.

This field of the Intent is a ComponentName object, which you can specify using a fully qualified class name of the target component, including the package name of the app, for example, com.example.ExampleActivity. You can set the component name with setComponent(), setClass(), setClassName(), or with the Intent constructor.

* **Action ** 
A string that specifies the generic action to perform (such as view or pick).
In the case of a broadcast intent, this is the action that took place and is being reported. The action largely determines how the rest of the intent is structured—particularly the information that is contained in the data and extras.You can specify your own actions for use by intents within your app (or for use by other apps to invoke components in your app), but you usually specify action constants defined by the Intent class or other framework classes. Here are some common actions for starting an activity:
* * ** ACTION_VIEW **
Use this action in an intent with startActivity() when you have some information that an activity can show to the user, such as a photo to view in a gallery app, or an address to view in a map app.
* * ** ACTION_SEND **
Also known as the share intent, you should use this in an intent with startActivity() when you have some data that the user can share through another app, such as an email app or social sharing app.

See the Intent class reference for more constants that define generic actions. Other actions are defined elsewhere in the Android framework, such as in Settings for actions that open specific screens in the system's Settings app. 

You can specify the action for an intent with setAction() or with an Intent constructor.
If you define your own actions, be sure to include your app's package name as a prefix,

* ** Data **
The URI (a Uri object) that references the data to be acted on and/or the MIME type of that data. The type of data supplied is generally dictated by the intent's action. For example, if the action is ACTION_EDIT, the data should contain the URI of the document to edit. When creating an intent, it's often important to specify the type of data (its MIME type) in addition to its URI. For example, an activity that's able to display images probably won't be able to play an audio file, even though the URI formats could be similar. Specifying the MIME type of your data helps the Android system find the best component to receive your intent. However, the MIME type can sometimes be inferred from the URI—particularly when the data is a content: URI. A content: URI indicates the data is located on the device and controlled by a ContentProvider, which makes the data MIME type visible to the system.To set only the data URI, call **setData()**. To set only the MIME type, call **setType()**. If necessary, you can set both explicitly with **setDataAndType()**. ** Caution: ** If you want to set both the URI and MIME type, don't call **setData()** and **setType()** because they each nullify the value of the other. Always use **setDataAndType()** to set both URI and MIME type.

* ** Category **
A string containing additional information about the kind of component that should handle the intent. Any number of category descriptions can be placed in an intent, but most intents do not require a category. Here are some common categories:
* * ** CATEGORY_BROWSABLE **
The target activity allows itself to be started by a web browser to display data referenced by a link, such as an image or an e-mail message.
* * **CATEGORY_LAUNCHER**
The activity is the initial activity of a task and is listed in the system's application launcher.

See the Intent class description for the full list of categories. You can specify a category with **addCategory()**.


* ** Extras **
Key-value pairs that carry additional information required to accomplish the requested action. Just as some actions use particular kinds of data URIs, some actions also use particular extras.
You can add extra data with various ** putExtra() ** methods, each accepting two parameters: the key name and the value. You can also create a Bundle object with all the extra data, then insert the Bundle in the Intent with ** putExtras() **. 

For example, when creating an intent to send an email with ** ACTION_SEND **, you can specify the to recipient with the ** EXTRA_EMAIL ** key, and specify the subject with the ** EXTRA_SUBJECT ** key.

The Intent class specifies many EXTRA_* constants for standardized data types. If you need to declare your own extra keys (for intents that your app receives), be sure to include your app's package name as a prefix, as shown in the following example:

```
#!java


static final String EXTRA_GIGAWATTS = "com.example.EXTRA_GIGAWATTS";
```

** Caution: ** Do not use Parcelable or Serializable data when sending an intent that you expect another app to receive. If an app attempts to access data in a Bundle object but does not have access to the parceled or serialized class, the system raises a RuntimeException.

* ** Flags ** Flags are defined in the Intent class that function as metadata for the intent. The flags may instruct the Android system how to launch an activity (for example, which task the activity should belong to) and how to treat it after it's launched (for example, whether it belongs in the list of recent activities).

### Receiving an implicit intent ###


An app component should declare separate filters for each unique job it can do. For example, one activity in an image gallery app may have two filters: one filter to view an image, and another filter to edit an image. When the activity starts, it inspects the Intent and decides how to behave based on the information in the Intent (such as to show the editor controls or not).

Each intent filter is defined by an <intent-filter> element in the app's manifest file, nested in the corresponding app component (such as an <activity> element). Inside the <intent-filter>, you can specify the type of intents to accept using one or more of these three elements:

* ** <action>** Declares the intent action accepted, in the name attribute. The value must be the literal string value of an action, not the class constant.
* ** <data> ** Declares the type of data accepted, using one or more attributes that specify various aspects of the data URI (scheme, host, port, path) and MIME type.
* ** <category> ** Declares the intent category accepted, in the name attribute. The value must be the literal string value of an action, not the class constant.

**Note**: To receive implicit intents, you **must include the CATEGORY_DEFAULT ** category in the intent filter. The methods ** startActivity() ** and ** startActivityForResult() ** treat all intents as if they declared the CATEGORY_DEFAULT category. If you do not declare this category in your intent filter, no implicit intents will resolve to your activity.

For example, here's an activity declaration with an intent filter to receive an ACTION_SEND intent when the data type is text:


```
#!xml

<activity android:name="ShareActivity">
    <intent-filter>
        <action android:name="android.intent.action.SEND"/>
        <category android:name="android.intent.category.DEFAULT"/>
        <data android:mimeType="text/plain"/>
    </intent-filter>
</activity>
```

You can create a filter that includes more than one instance of ** <action> **, ** <data> **, or ** <category> **. If you do, you need to be certain that the component can handle any and all combinations of those filter elements.

When you want to handle multiple kinds of intents, but only in specific combinations of action, data, and category type, then you need to create multiple intent filters.

An implicit intent is tested against a filter by comparing the intent to each of the three elements. To be delivered to the component, the intent must pass all three tests. If it fails to match even one of them, the Android system won't deliver the intent to the component. However, because a component may have multiple intent filters, an intent that does not pass through one of a component's filters might make it through on another filter

### Intent resolution ### 

* ** Action test ** To pass this filter, the action specified in the Intent must match one of the actions listed in the filter. If the filter does not list any actions, there is nothing for an intent to match, so all intents fail the test. However, if an Intent does not specify an action, it passes the test as long as the filter contains at least one action.

* ** Category test ** For an intent to pass the category test, every category in the Intent must match a category in the filter. The reverse is not necessary—the intent filter may declare more categories than are specified in the Intent and the Intent still passes. Therefore, an intent with no categories always passes this test, regardless of what categories are declared in the filter. Android automatically applies the ** CATEGORY_DEFAULT ** category to all implicit intents passed to ** startActivity() ** and ** startActivityForResult() **. If you want your activity to receive implicit intents, it must include a category for 
```
#!xml

"android.intent.category.DEFAULT"
```
 in its intent filters

* ** Data test ** Each of these attributes is optional in a <data> element, but there are linear dependencies:

* * If a scheme is not specified, the host is ignored.
* * If a host is not specified, the port is ignored.
* * If both the scheme and host are not specified, the path is ignored.
When the URI in an intent is compared to a URI specification in a filter, it's compared only to the parts of the URI included in the filter. For example:

* * If a filter specifies only a scheme, all URIs with that scheme match the filter.
* * If a filter specifies a scheme and an authority but no path, all URIs with the same scheme and authority pass the filter, regardless of their paths.
* * If a filter specifies a scheme, an authority, and a path, only URIs with the same scheme, authority, and path pass the filter.

The data test compares both the URI and the MIME type in the intent to a URI and MIME type specified in the filter. The rules are as follows:

* * An intent that contains neither a URI nor a MIME type passes the test only if the filter does not specify any URIs or MIME types.
* * An intent that contains a URI but no MIME type (neither explicit nor inferable from the URI) passes the test only if its URI matches the filter's URI format and the filter likewise does not specify a MIME type.
* * An intent that contains a MIME type but not a URI passes the test only if the filter lists the same MIME type and does not specify a URI format.
* * An intent that contains both a URI and a MIME type (either explicit or inferable from the URI) passes the MIME type part of the test only if that type matches a type listed in the filter. It passes the URI part of the test either if its URI matches a URI in the filter or if it has a content: or file: URI and the filter does not specify a URI. In other words, a component is presumed to support content: and file: data if its filter lists only a MIME type.



### Pending Intent ###

A PendingIntent object is a wrapper around an Intent object. The primary purpose of a PendingIntent is to grant permission to a foreign application to use the contained Intent as if it were executed from your app's own process.

Major use cases for a pending intent include the following:

* Declaring an intent to be executed when the user performs an action with your Notification (the Android system's NotificationManager executes the Intent).
* Declaring an intent to be executed when the user performs an action with your App Widget (the Home screen app executes the Intent).
* Declaring an intent to be executed at a specified future time (the Android system's AlarmManager executes the Intent).


## [Content Providers](https://developer.android.com/guide/topics/providers/content-provider-basics.html) ##

Content providers can help an application manage access to data stored by itself, stored by other apps, and provide a way to share data with other apps. They encapsulate the data, and provide mechanisms for defining data security. Content providers are the standard interface that connects data in one process with code running in another process. Implementing a content provider has many advantages. Most importantly you can configure a content provider to allow other applications to securely access and modify your app data

![content-provider-overview.png](https://bitbucket.org/repo/oGdnjo/images/3848991199-content-provider-overview.png)

Typically you work with content providers in one of two scenarios; you may want to implement code to access an exiting content provider in another application, or you may want to create a new content provider in your application to share data with other applications.

A content provider presents data to external applications as one or more tables that are similar to the tables found in a relational database. A row represents an instance of some type of data the provider collects, and each column in the row represents an individual piece of data collected for an instance.

The ContentResolver methods provide the basic "CRUD" (create, retrieve, update, and delete) functions of persistent storage.

## [Broadcast Receivers](https://developer.android.com/reference/android/content/BroadcastReceiver.html) ##
Android apps can send or receive broadcast messages from the Android system and other Android apps, similar to the publish-subscribe design pattern. These broadcasts are sent when an event of interest occurs. For example, the Android system sends broadcasts when various system events occur, such as when the system boots up or the device starts charging. Apps can also send custom broadcasts, for example, to notify other apps of something that they might be interested in.

Apps can register to receive specific broadcasts. When a broadcast is sent, the system automatically routes broadcasts to apps that have subscribed to receive that particular type of broadcast.


There are two major classes of broadcasts that can be received:

* ** Normal ** broadcasts (sent with ** Context.sendBroadcast **) are completely asynchronous. All receivers of the broadcast are run in an undefined order, often at the same time. This is more efficient, but means that receivers cannot use the result or abort APIs included here.

* ** Ordered ** broadcasts (sent with ** Context.sendOrderedBroadcast **) are delivered to one receiver at a time. As each receiver executes in turn, it can propagate a result to the next receiver, or it can completely abort the broadcast so that it won't be passed to other receivers. The order receivers run in can be controlled with the android:priority attribute of the matching intent-filter; receivers with the same priority will be run in an arbitrary order.

### Security
* The Intent namespace is global. Make sure that Intent action names and other strings are written in a namespace you own, or else you may inadvertently conflict with other applications.
* When you use registerReceiver(BroadcastReceiver, IntentFilter), any application may send broadcasts to that registered receiver. You can control who can send broadcasts to it through permissions described below.
* When you publish a receiver in your application's manifest and specify intent-filters for it, any other application can send broadcasts to it regardless of the filters you specify. To prevent others from sending to it, make it unavailable to them with android:exported="false".
* When you use sendBroadcast(Intent) or related methods, normally any other application can receive these broadcasts. You can control who can receive such broadcasts through permissions described below. Alternatively, starting with ICE_CREAM_SANDWICH, you can also safely restrict the broadcast to a single application with Intent.setPackage

None of these issues exist when using LocalBroadcastManager, since intents broadcast it never go outside of the current process.


### Receiver Lifecycle
A BroadcastReceiver object is only valid for the duration of the call to onReceive(Context, Intent). Once your code returns from this function, the system considers the object to be finished and no longer active.

This has important repercussions to what you can do in an onReceive(Context, Intent) implementation: anything that requires asynchronous operation is not available, because you will need to return from the function to handle the asynchronous operation, but at that point the BroadcastReceiver is no longer active and thus the system is free to kill its process before the asynchronous operation completes.

### Process Lifecycle
A process that is currently executing a BroadcastReceiver (that is, currently running the code in its onReceive(Context, Intent) method) is considered to be a foreground process and will be kept running by the system except under cases of extreme memory pressure.

Once you return from onReceive(), the BroadcastReceiver is no longer active, and its hosting process is only as important as any other application components that are running in it. This is especially important because if that process was only hosting the BroadcastReceiver (a common case for applications that the user has never or not recently interacted with), then upon returning from onReceive() the system will consider its process to be empty and aggressively kill it so that resources are available for other more important processes.

This means that for longer-running operations you will often use a Service in conjunction with a BroadcastReceiver to keep the containing process active for the entire time of your operation.








## [Processes and Threads:](https://developer.android.com/guide/components/processes-and-threads.html) ##

When an application component starts and the application does not have any other components running, the Android system starts a new Linux process for the application with a single thread of execution. By default, all components of the same application run in the same process and thread (called the "main" thread). If an application component starts and there already exists a process for that application (because another component from the application exists), then the component is started within that process and uses the same thread of execution. However, you can arrange for different components in your application to run in separate processes, and you can create additional threads for any process.

### Processes ###

By default, all components of the same application run in the same process and most applications should not change this. However, if you find that you need to control which process a certain component belongs to, you can do so in the manifest file.

The manifest entry for each type of component element—<activity>, <service>, <receiver>, and <provider>—supports an android:process attribute that can specify a process in which that component should run. You can set this attribute so that each component runs in its own process or so that some components share a process while others do not. You can also set android:process so that components of different applications run in the same process—provided that the applications share the same Linux user ID and are signed with the same certificates.

The <application> element also supports an android:process attribute, to set a default value that applies to all components.

### Process lifecycle ###

1) **Foreground process**
A process that is required for what the user is currently doing. A process is considered to be in the foreground if any of the following conditions are true:

* It hosts an Activity that the user is interacting with (the Activity's onResume() method has been called).
* It hosts a Service that's bound to the activity that the user is interacting with.
* It hosts a Service that's running "in the foreground"—the service has called startForeground().
* It hosts a Service that's executing one of its lifecycle callbacks (onCreate(), onStart(), or onDestroy()).
* It hosts a BroadcastReceiver that's executing its onReceive() method.

Generally, only a few foreground processes exist at any given time. They are killed only as a last resort—if memory is so low that they cannot all continue to run. Generally, at that point, the device has reached a memory paging state, so killing some foreground processes is required to keep the user interface responsive.

2) **Visible process**
A process that doesn't have any foreground components, but still can affect what the user sees on screen. A process is considered to be visible if either of the following conditions are true:

* It hosts an Activity that is not in the foreground, but is still visible to the user (its onPause() method has been called). This might occur, for example, if the foreground activity started a dialog, which allows the previous activity to be seen behind it.
* It hosts a Service that's bound to a visible (or foreground) activity.

A visible process is considered extremely important and will not be killed unless doing so is required to keep all foreground processes running.

3) ** Service process **
A process that is running a service that has been started with the startService() method and does not fall into either of the two higher categories. Although service processes are not directly tied to anything the user sees, they are generally doing things that the user cares about (such as playing music in the background or downloading data on the network), so the system keeps them running unless there's not enough memory to retain them along with all foreground and visible processes.
4) ** Background process **
A process holding an activity that's not currently visible to the user (the activity's onStop() method has been called). These processes have no direct impact on the user experience, and the system can kill them at any time to reclaim memory for a foreground, visible, or service process. Usually there are many background processes running, so they are kept in an LRU (least recently used) list to ensure that the process with the activity that was most recently seen by the user is the last to be killed. If an activity implements its lifecycle methods correctly, and saves its current state, killing its process will not have a visible effect on the user experience, because when the user navigates back to the activity, the activity restores all of its visible state. See the Activities document for information about saving and restoring state.
5) ** Empty process **
A process that doesn't hold any active application components. The only reason to keep this kind of process alive is for caching purposes, to improve startup time the next time a component needs to run in it. The system often kills these processes in order to balance overall system resources between process caches and the underlying kernel caches.

In addition, a process's ranking might be increased because other processes are dependent on it—a process that is serving another process can never be ranked lower than the process it is serving. For example, if a content provider in process A is serving a client in process B, or if a service in process A is bound to a component in process B, process A is always considered at least as important as process B.

Because a process running a service is ranked higher than a process with background activities, an activity that initiates a long-running operation might do well to start a service for that operation, rather than simply create a worker thread—particularly if the operation will likely outlast the activity. For example, an activity that's uploading a picture to a web site should start a service to perform the upload so that the upload can continue in the background even if the user leaves the activity. Using a service guarantees that the operation will have at least "service process" priority, regardless of what happens to the activity. This is the same reason that broadcast receivers should employ services rather than simply put time-consuming operations in a thread.

Additionally, the Andoid UI toolkit is not thread-safe. So, you must not manipulate your UI from a worker thread—you must do all manipulation to your user interface from the UI thread. Thus, there are simply two rules to Android's single thread model:

1. Do not block the UI thread
2. Do not access the Android UI toolkit from outside the UI thread





Handles
Loopers
AsyncTask/Loaders

## [Security](https://developer.android.com/training/best-security.html)


## Notifications ##:

## AIDL ##

## Serializable/Parcelable ##

## Layouts ##
Relative
Linear 
Frame

## Screen orientation ##

## Permissions ##

## RecyclerView ##

## Screen Sizes ##


Frameworks

## GreenDao ##
## RxJava ##
## Dagger2 ##


## JobScheduler(v21) ##